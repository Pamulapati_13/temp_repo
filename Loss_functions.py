# -*- coding: utf-8 -*-
"""
Created on Sat Apr 25 07:47:28 2020
This file containes all the loss functions
@author: test
"""
import tensorflow as tf
import numpy as np

# Losses for the generator
cross_entrpy = tf.keras.losses.BinaryCrossentropy(from_logits = True)
#GAN generator loss
def gan_loss_g(disc_gen_op):
    return cross_entrpy(tf.zeros_like(disc_gen_op),disc_gen_op)

#The content loss also applies for the generator.
#Content loss has two parts: MSE loss and feature loss.
#MSE loss: This is the L2 distance between the real and generated images.
def mse_loss(gen_img,real_img):
    return tf.reduce_mean(tf.keras.losses.MSE(gen_img,real_img))
#CNN loss.
#We take the intermediate output of the generator.
def CNN_loss(gen_disc_op,real_disc_op):
    return tf.reduce_mean(tf.keras.losses.MSE(gen_disc_op,real_disc_op)) 

#Triplet loss from the second pair.
    
def triplet_loss(anchor,prev,real):
    sh = anchor.shape
    #the size of the image from the previous state is less than the the one from this state.
    #We need to reshape it to match the size.
    prev = tf.image.resize(prev,(sh[1],sh[2]))
    part1 = tf.math.squared_difference(anchor,real)
    part2 = tf.math.squared_difference(anchor,prev)
    total= tf.reduce_sum(part1-part2)
    
    return total  
 

# Discriminator loss

def disc_gan_loss(disc_gen_op,disc_real_op):
    real = cross_entrpy(tf.zeros_like(disc_real_op),disc_real_op)
    gen = cross_entrpy(tf.zeros_like(disc_gen_op),disc_gen_op)
    total = real+gen
    
    return total

# Wesserstien Loss

def disc_wes_loss(disc_gen_op,disc_real_op):
    l = tf.reduce_mean(disc_gen_op) - tf.reduce_mean(disc_real_op)
    return l

# Total loss
# We are having weigths for each loss.
# We will include the triplet loss seperately with a weight.

def total_loss(disc_gen_op,disc_real_op,gen_img,real_img,weights = [1,1,1]):
    g_loss = weights[0] * gan_loss_g(disc_gen_op) + weights[1] * mse_loss(gen_img,real_img) + weights[2] * CNN_loss(disc_gen_op,disc_real_op)
    
    d_loss  = disc_gan_loss(disc_gen_op, disc_real_op)
    
    return g_loss,d_loss
    
    
