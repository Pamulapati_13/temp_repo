# -*- coding: utf-8 -*-
"""
Created on Sun Apr  5 20:29:26 2020

@author: test
"""
#This file contains all the functions required for predictions and measuring performance of the model.

import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np

def generate_img(generator_models,ip_image,intermediate_op = False, want_op = False,target_shape = (64,64)):
    
    #in the future we will give pass the target_shape as well
    
    #Here we will pass all the models in a list
    #We should only pass generator models for prediction. 
    #We can also look at the outputs of the intermediate output.
    #This is controlled by the intermediate_op variable
    
    #We want to see if the shape of the image is of this format:[?,l,b,c]
    if len(ip_image.shape) != 4:
        tf.reshape(ip_image,[1,ip_image.shape[0],ip_image.shape[1],1])
    no = 0
    ip = ip_image
    for gen in generator_models:
        op = gen(ip_image)
        ip = op
        no+=1
        if no = len(generator_models)-2:
            ip = tf.reshape(ip,[1,target_shape[0],target_shape[1],1])
            
    plt.imshow(temp_op)
    plt.show()
    
    if want_op:
        return temp_op
