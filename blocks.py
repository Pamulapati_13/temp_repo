# -*- coding: utf-8 -*-
# This file contains all the bulding blocks required to build the model.

import tensorflow as tf

def residual_block(filters,kernal_size=(3,3),stride = (1,1), apply_bn = True):
    model = tf.keras.Sequential()
    model.add(tf.keras.layers.SeparableConv2D(filters,kernal_size,stride,padding = 'same'))
    if apply_bn:
        model.add(tf.keras.layers.BatchNormalization(epsilon = 6e-8))
    model.add(tf.keras.layers.PReLU())
    model.add(tf.keras.layers.SeparableConv2D(filters,kernal_size,stride,padding = 'same'))
    if apply_bn:
        model.add(tf.keras.layers.BatchNormalization(epsilon = 6e-8))
    model.add(tf.keras.layers.PReLU())
    
    return model

def discriminator_block(filters,kernal_size = (3,3), apply_bn = True):
    model = tf.keras.Sequential()
    model.add(tf.keras.layers.SeparableConv2D(filters,kernal_size,2,padding = 'same'))
    model.add(tf.keras.layers.LeakyReLU())
    if apply_bn:
        model.add(tf.keras.layers.BatchNormalization(epsilon = 6e-8))
    
    return model

