# -*- coding: utf-8 -*-
"""
Created on Mon Mar 30 18:35:57 2020

@author: Sudeep Pamulapati

This file contains all the functions to read the data from tfrecords file.
"""

import tensorflow as tf
import matplotlib.pyplot as plt
import numpy


# This is the parser function
# flt_fs is the size of the input image we want to read.
# This will be either 300x300 or 64x64
def parser(flt_fs):
    def wrapper(in_example_protos):
        return _parse_SeqExampleProto_zip_crop_flip_v4(flt_fs, in_example_protos)
  
    return wrapper 

def _parse_SeqExampleProto_zip_crop_flip_v4(flt_fs, in_example_protos):
    features_dict = {
    'ARD_LST_ref': tf.io.FixedLenFeature(shape=[flt_fs * flt_fs], dtype=tf.float32),
    # _float_feature(ARD_LST_ref[iiBatch,:,:].reshape((-1))),
    'ARD_LST_tgt_masked': tf.io.FixedLenFeature(shape=[flt_fs * flt_fs], dtype=tf.float32),
    # _float_feature(ARD_LST_tgt_masked[iiBatch,:,:].reshape((-1))),
    'Mask_Img': tf.io.FixedLenFeature(shape=[flt_fs * flt_fs], dtype=tf.float32),
    'ref_tgt_DOY_difference': tf.io.FixedLenFeature(shape=[], dtype=tf.float32),
    # _float_feature(ref_tgt_DOY_difference[iiBatch]),
    'ref_DOY': tf.io.FixedLenFeature(shape=[], dtype=tf.float32),  # _float_feature(ref_DOY[iiBatch]),
    'tgt_DOY': tf.io.FixedLenFeature(shape=[], dtype=tf.float32),  # _float_feature(tgt_DOY[iiBatch]),
    'ref_tgt_DOY_difference_reverse': tf.io.FixedLenFeature(shape=[], dtype=tf.float32),
    # _float_feature(ref_tgt_DOY_difference_reverse[iiBatch]),
    # 'mean_feature': tf.io.FixedLenFeature(shape = [], dtype=tf.float32), #_float_feature(mean_featureNP[iiBatch]),
    
    'ARD_LST_tgt': tf.io.FixedLenFeature(shape=[flt_fs * flt_fs], dtype=tf.float32)
    # _float_feature(ARD_LST_tgt[iiBatch,:,:].reshape((-1)))
    }
    
    #This will read all the images
    features = tf.io.parse_example(in_example_protos,features_dict)
    
    #We only need images of this feature name
    
    ARD_LST_ref1D = features["ARD_LST_ref"]
    ARD_LST_ref = tf.reshape(ARD_LST_ref1D, [-1, flt_fs, flt_fs,1])
    ARD_LST_gen1_ip = tf.image.resize(ARD_LST_ref,(9,9))
    ARD_LST_gen2_ip = tf.image.resize(ARD_LST_ref,(18,18))
    ARD_LST_gen2_op = tf.image.resize(ARD_LST_ref,(36,36))
    
    return (ARD_LST_gen1_ip,ARD_LST_gen2_ip,ARD_LST_gen2_op,ARD_LST_ref)

#This function can be used to load the data.
    
def load_data(data_loc,flt_fs,batch_size,buffer_size = 1):
    modified_file_loc = data_loc + '/*gz' #all the files will be zipped
    dataset = tf.data.Dataset.list_files(modified_file_loc)
    dataset = dataset.interleave(
        lambda x: tf.data.TFRecordDataset(x,compression_type="GZIP",num_parallel_reads=1),
        cycle_length=-1,
        block_length=1,
        num_parallel_calls=tf.data.experimental.AUTOTUNE)
    dataset = dataset.batch(batch_size, drop_remainder = True)
    dataset = dataset.map(parser(flt_fs), num_parallel_calls=tf.data.experimental.AUTOTUNE)
    dataset = dataset.prefetch(buffer_size)
    
    return dataset

# the above function will give a tuple of images each having a batch size of 16.
    
    
    

