# -*- coding: utf-8 -*-
"""
Created on Mon Mar 16 19:58:44 2020

@author: test
"""
## Here we will write the training loop
import tensorflow as tf
import numpy as np
from singlegan import Generator,Discriminator,total_loss
import time
import os
from data_loader import load_data
from predict import generate_img

#--------------------------------------------------------------------------------------------------------------
#                                   Load the data here
#--------------------------------------------------------------------------------------------------------------

#Place left vacent to load the data
data_loc = 'add you data location here'
flt_fs = 64
batch_size = 16
ground_truth = load_data(data_loc,flt_fs,batch_size)

#See that everything is a 4D tensor

ground_truth = ground_truth.map(lambda image:tf.reshape(image(batch_size,flt_fs,flt_fs,1)))
# Data for Generator 1
gen1_ip = ground_truth.map(lambda image:tf.image.resize(image,(9,9)))
#Data for Generator 2
gen2_ip = ground_truth.map(lambda image:tf.image.resize(image,(18,18)))
#Data for Generator 3
gen2_op = ground_truth.map(lambda image:tf.image.resize(image,(36,36)))


#--------------------------------------------------------------------------------------------------------------
#                                   Define Models and optimizersds
#--------------------------------------------------------------------------------------------------------------

upscale_factor = 8
input_shape = #define the input shape of the pgan here
target_shape = #Define the target shape here. This is the output shape you are expecting

generator_models,generator_optimizers,discriminator_models,discriminator_optimizers = build_pgan(upscale_factor,input_shape,target_shape)

##------------------------------------------------------------------------------------
#                       We are defining Custom callbacks here.
##------------------------------------------------------------------------------------

#this will save the intermediate layer distributions
writer = tf.summary.writer('./log/')
def write_log(model,epoch):
    with writer.as_default():
        for layers in model.layers:
            tf.summary.histogram('Weights',layer.get_weights()[0],step = epoch)
            tf.summary.histogram('bias',layer.get_weights()[1],step = epoch)

#-------------------------------------------------------------------------------------
# This is the function for a train step.
@tf.function()
def train_stepV2(input_img,target):
    generator_loss = []
    discriminator_loss = []
    ip = input_img
    no = 0
    with tf.GradientTape(persistant = True) as t:
        for gen,disc,gen_opt,disc_opt in zip(generator_models,discriminator_models,generator_optimizers,discriminator_optimizers):
            gen_op = gen(ip)
            disc_gen_op = disc(gen_op)
            disc_real_op = disc(target[no])
            g_loss,d_loss = total_loss(no,ip,gen_op,disc_gen_op,disc_real_op,target[no])
            g_gradients = t.gradient(g_loss,gen.trainable_variables)
            d_gradients = t.gradient(d_loss,disc.trainable_variables)
            gen_opt.apply_gradients(zip(g_gradients,generator_1.trainable_variables))
            disc_opt.apply_gradients(zip(d_gradients,discriminator_1.trainable_variables))
            ip = gen_op
            no+=1
            if no == len(generator_models)-2:
                ip = tf.resize(ip,(target.shape[1]/2,target.shape[2]/2))
                
#This will validate the model and produce the output of the last test batch.  
# Put all the validation data in a tuple and pass to  the function. 
#This is for the sake of writing the loop easily.
                
def validate_model(generator_models,discriminator_models,validation_data,disp = False):
    for a,b,c,d in tf.data.Dataset.zip(validation_data):
        ip = a
        op = [b,c,d]
        gen_loss_tracker = [tf.keras.metrics.Mean()]*4
        disc_loss_tracker = [tf.keras.metrics.Mean()]*4
        
        for gen,disc,gen_loss,disc_loss in zip(generator_models,discriminator_models,gen_loss_tracker,disc_loss_tracker):
            gen_op = gen(ip)
            disc_gen_op = disc(gen_op)
            disc_real_op = disc(target[no])
            g_loss,d_loss = total_loss(no,ip,gen_op,disc_gen_op,disc_real_op,target[no])
            gen_loss.update_state(g_loss)
            disc_loss.update_state(d_loss)
            ip = gen_op
            
            no+=1
            if no == len(generator_models)-2:
                ip = tf.resize(ip,(target.shape[1]/2,target.shape[2]/2))
                
        #Here we will display the output of the last validation batch.
        #We can enable or disable this.
                
    if disp:
        predict(a[0,:,:,:])
        
    return [gen_loss_tracker,disc_loss_tracker]
    
            
        

               
#Now lets train the model
#we can just save the architecture with out weights into a JSON file.
# generator_1.to_json()
# generator_2.to_json()
# generator_3.to_json()
        
# discriminator_1.to_json()
# discriminator_2.to_json()
# discriminator_3.to_json()

for epoch in range(5):
    n = 0
    for low,high in tf.data.Dataset.zip(((gen1_ip,gen2_ip,gen2_op,ground_truth))):
        train_step(low, intermediate, high_img)
        if n%10 == 0:
            print('.',end = '')
        n+=1
    
    #We are saving the model distributions
    write_log(generator_1, epoch)
    write_log(generator_2, epoch)
    write_log(generator_3, epoch)
    
    write_log(discriminator_1, epoch)
    write_log(discriminator_2, epoch)
    write_log(discriminator_3, epoch)
    
    
    #save the checkpoints for every 5 iterations
    #checkpoints only save weights
    # We can use the json models to use these weights
    if (epoch+1)%5==0:
        checkpoint1.save(file_prefix = checkpoint_prefix1)
        checkpoint2.save(file_prefix = checkpoint_prefix2)
        checkpoint3.save(file_prefix = checkpoint_prefix3)
    #We can save the entire models and weights like this:
    # generator_1.save(generator_1.h5)
    # generator_2.save(generator_2.h5)
    # generator_3.save(generator_3.h5)
        
    # discriminator_1.save(discriminator_1.h5)
    # discriminator_2.save(discriminator_2.h5)
    # discriminator_3.save(discriminator_3.h5)
    
