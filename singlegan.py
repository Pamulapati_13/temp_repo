# -*- coding: utf-8 -*-
"""
@author: Sudeep Pamulapati
"""
# This file contains generator and discriminator Functions and required loss functions.
import tensorflow as tf
from blocks import residual_block,discriminator_block
import math

initializer = tf.keras.initializers.he_normal()
regularizer = tf.keras.regularizers.l2(l = 6e-4)

def Generator(input_shape):
    inputs = tf.keras.layers.Input(shape = input_shape)
    lay1 = tf.keras.layers.SeparableConv2D(64,(9,9),(1,1),padding = 'same',
        depthwise_regularizer = regularizer,pointwise_regularizer = regularizer,
        pointwise_initializer = initializer,depthwise_initializer = initializer)(inputs)
    act1 = tf.keras.layers.PReLU()(lay1)
    lay2 = residual_block(filters = 64)(act1)
    add1 = tf.keras.layers.add([act1,lay2])
    lay3 = residual_block(filters = 64)(add1)
    add2 = tf.keras.layers.add([add1,lay3])
    lay4 = residual_block(filters = 64)(add2)
    add3 = tf.keras.layers.add([add2,lay4])
    lay5 = residual_block(filters = 64)(add3)
    add4 = tf.keras.layers.add([add3,lay5])
    lay6 = residual_block(filters = 64)(add4)
    add5 = tf.keras.layers.add([add4,lay6])
    lay7 = tf.keras.layers.SeparableConv2D(64,(3,3),(1,1),padding = 'same',
        depthwise_regularizer = regularizer,pointwise_regularizer = regularizer,
        pointwise_initializer = initializer,depthwise_initializer = initializer)(add5)
    lay8 = tf.keras.layers.BatchNormalization(epsilon = 6e-8)(lay7)
    add6 = tf.keras.layers.add([act1,lay8])
    # Here we have the upsampling block
    lay9 = tf.keras.layers.SeparableConv2D(256,(3,3),(1,1),padding = 'same',
        depthwise_regularizer = regularizer,pointwise_regularizer = regularizer,
        pointwise_initializer = initializer,depthwise_initializer = initializer)(add6)
    sub_layer = tf.keras.layers.Lambda(lambda x:tf.nn.depth_to_space(x,2))
    lay10 = sub_layer(inputs = lay9)
    lay11 = tf.keras.layers.PReLU()(lay10)
    #end of the upsampling block
    lay12 = tf.keras.layers.SeparableConv2D(1,(9,9),(1,1),padding = 'same',
        depthwise_regularizer = regularizer,pointwise_regularizer = regularizer,
        pointwise_initializer = initializer,depthwise_initializer = initializer)(lay11)
    
    return tf.keras.Model(inputs = inputs,outputs = lay12)

#The size of the generator remains same in all the GANs. 
#But the size of the discriminator has to be changed based.
#The size has to be changed based on the input image    

def Discriminator(input_shape, disc_no,lay_op_num = 3):
    
    inputs = tf.keras.layers.Input(shape = input_shape)
    blk1 = discriminator_block(64)(inputs)
    blk2 = discriminator_block(64)(blk1)
    blk3 = discriminator_block(128)(blk2)
    blk = discriminator_block(128)(blk3)
    #Our model has multiple discriminators and generators. 
    #The architecture of the generator remains fairly the same.
    #But the size of the discriminator has to be increased at every stage.
    #So, this for loop increase the size of the discriminator when the number is increased.
    if disc_no >1:
        for i in range(1,math.ceil(disc_no/2)):
            blk = discriminator_block(128*(2**i))(blk)
            blk = discriminator_block(128*(2**i))(blk)
    flt = tf.keras.layers.Flatten()(blk)
    #We changed the number of units in the dense layer to 512 from 1024.
    #As this layer has more parameters that the other layers togeather.
    den1 = tf.keras.layers.Dense(512, kernel_initializer = initializer,
                                 kernel_regularizer = regularizer)(flt)
    act1 = tf.keras.layers.LeakyReLU()(den1)
    final = tf.keras.layers.Dense(1)(act1)
    
    op_list = [final,blk1,blk2,blk3]
    
    
    return tf.keras.Model(inputs = inputs, outputs = [op_list[0],op_list[lay_op_num]])



    
# Experimental
#This is a function to build the pgan in total.
def build_pgan(upscale_factor,input_shape,target_shape,lr_rate=1e-6):
    generator_models = []
    dicriminator_models = []
    generator_optimizers = []
    discriminator_optimizers = []
    i = upscale_factor
    while i>2 :
        generator_models.append(Generator((input_shape[0],input_shape[1],1)))
        generator_optimizers.append(tf.keras.optimizers.Adam(lr_rate))
        discriminator_models.append(Discriminator((input_shape[0]*2,input_shape[1]*2,1)))
        discriminator_optimizers.append(tf.keras.optimizers.Adam(lr_rate))
        input_shape = [input_shape[0]*2,input_shape[1]*2]
        i = i/2

    generator_models.append(Generator((target_shape[0]/2,target_shape[1]/2,1)))
    generator_optimizers.append(tf.keras.optimizers.Adam(lr_rate))
    discriminator_models.append(Discriminator((target_shape[0],target_shape[1],1)))
    discriminator_optimizers.append(tf.keras.optimizers.Adam(lr_rate))

    return [generator_models,generator_optimizers,dicriminator_models,discriminator_optimizers]