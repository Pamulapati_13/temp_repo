# -*- coding: utf-8 -*-
"""
Created on Mon Mar 16 19:58:44 2020

@author: test
"""
## Here we will write the training loop
import tensorflow as tf
import numpy as np
from singlegan import Generator,Discriminator,gan_loss_g,mse_loss,CNN_loss,disc_gan_loss,triplet_loss
import time
import os
from data_loader import load_data

#--------------------------------------------------------------------------------------------------------------
#                                   Load the data here
#--------------------------------------------------------------------------------------------------------------

#Place left vacent to load the data
data_loc = 'add you data location here'
flt_fs = 64
batch_size = 16
ground_truth = load_data(data_loc,flt_fs,batch_size)

#See that everything is a 4D tensor

ground_truth = ground_truth.map(lambda image:tf.reshape(image(batch_size,flt_fs,flt_fs,1)))
# Data for Generator 1
gen1_ip = ground_truth.map(lambda image:tf.image.resize(image,(9,9)))
#Data for Generator 2
gen2_ip = ground_truth.map(lambda image:tf.image.resize(image,(18,18)))
#Data for Generator 2
gen2_op = ground_truth.map(lambda image:tf.image.resize(image,(36,36)))



#--------------------------------------------------------------------------------------------------------------
#                                   Define Models and optimizers
#--------------------------------------------------------------------------------------------------------------
        
#1st generator and discriminator pair
#input: 9x9x1
#output: 18x18x1
generator_1 = Generator((9,9,1))
discriminator_1 = Discriminator((18,18,1), 1)
generator_1_optimizer = tf.keras.optimizers.Adam(2e-4, beta_1=0.5)   #Optimizers for first generator discriminator pairs
discriminator_1_optimizer = tf.keras.optimizers.Adam(2e-4, beta_1=0.5)
#checkpoints
checkpoint_dir1 = './pair_1_checkpoints'
checkpoint_prefix1 = os.path.join(checkpoint_dir1,"ckpt")
checkpoint1 = tf.train.Checkpoint(generator_optimizer=generator_1_optimizer,
                                 discriminator_optimizer=discriminator_1_optimizer,
                                 generator=generator_1,
                                 discriminator=discriminator_1)

#2nd Pair
#input: 18x18x1
#output: 36x36x1
generator_2 = Generator((18,18,1))
discriminator_2 = Discriminator((36,36,1),2)
generator_2_optimizer = tf.keras.optimizers.Adam(2e-4, beta_1=0.5)
discriminator_2_optimizer = tf.keras.optimizers.Adam(2e-4, beta_1=0.5)
#checkpoints
checkpoint_dir2= './pair_2_checkpoints'
checkpoint_prefix2 = os.path.join(checkpoint_dir2,"ckpt")
checkpoint2 = tf.train.Checkpoint(generator_optimizer=generator_2_optimizer,
                                 discriminator_optimizer=discriminator_2_optimizer,
                                 generator=generator_2,
                                 discriminator=discriminator_2)

#3rd Pair
#The target shape is 64x64, which means that we should have a shape of 32x32.
#We will rescale it.
generator_3 = Generator((32,32,1))
discriminator_3 = Discriminator((64,64,1), 3)
generator_3_optimizer = tf.keras.optimizers.Adam(2e-4, beta_1=0.5)
discriminator_3_optimizer = tf.keras.optimizers.Adam(2e-4, beta_1=0.5)
#checkpoints
checkpoint_dir3= './pair_3_checkpoints'
checkpoint_prefix3 = os.path.join(checkpoint_dir3,"ckpt")
checkpoint3 = tf.train.Checkpoint(generator_optimizer=generator_3_optimizer,
                                 discriminator_optimizer=discriminator_3_optimizer,
                                 generator=generator_3,
                                 discriminator=discriminator_3)

##------------------------------------------------------------------------------------
#                       We are defining Custom callbacks here.
##------------------------------------------------------------------------------------

#this will save the intermediate layer distributions
writer = tf.summary.writer('./log/')
def write_log(model,epoch):
    with writer.as_default():
        for layers in model.layers:
            tf.summary.histogram('Weights',layer.get_weights()[0],step = epoch)
            tf.summary.histogram('bias',layer.get_weights()[1],step = epoch)

#-------------------------------------------------------------------------------------
# This is the function for a train step.

@tf.function()
def train_step(input_img, prev_op,target):
    #Persistent has to be enabled because we are using the same gradient tape for different networks
    with tf.GradientTape(persistent = True) as t: 
        #outputs of 1st pair and corresponding losses
        gen_1 = generator_1(input_img)
        disc_1_real = discriminator_1(target[0])
        disc_1_gen = discriminator_1(gen_1)
        G1_loss = gan_loss_g(gen_1) + mse_loss(tf.reshape(gen_1,gen1_op_shape)) + CNN_loss(disc_1_gen[1], disc_1_real[1])
        D1_loss = disc_gan_loss(disc_1_gen[0], disc_1_real[0])
        
        #outputs of 2nd pair and corresponding losses
        gen_2 = generator_2(gen_1)
        disc_2_real = discriminator_2(target[1])
        disc_2_gen = discriminator_2(gen_2)
        G2_loss = gan_loss_g(gen_2) + mse_loss(tf.reshape(gen_2,gen2_op_shape))+ triplet_loss(gen_2, prev_op[0], target[1]) + CNN_loss(disc_2_gen[1], disc_2_real[1])
        D2_loss = disc_gan_loss(disc_2_gen[0], disc_2_real[0])
        
        #outputs of 3rd pair and corresponding losses
        #resize before feeding the image to the network
        gen_3_ip = tf.image.resize(gen_2,(32,32,1))  #Reducing the size of the image
        gen_3 = generator_3(gen_3_ip)
        disc_3_real = discriminator_3(target[2])
        disc_3_gen = discriminator_3(gen_3)
        G3_loss = gan_loss_g(gen_3) + mse_loss(tf.reshape(gen_3,gen3_op_shape)) + triplet_loss(gen_3, prev_op[1], target[2]) + CNN_loss(disc_3_gen[1], disc_3_real[1])
        D3_loss = disc_gan_loss(disc_3_gen[0], disc_3_real[0])
        
        #here we will obtain gradients of all the generators and discriminators
        G1_gradients = t.gradient(G1_loss,generator_1.trainable_variables)
        G2_gradients = t.gradient(G2_loss,generator_2.trainable_variables)
        G3_gradients = t.gradient(G3_loss,generator_3.trainable_variables)
        
        D1_gradients = t.gradient(D1_loss,discriminator_1.trainable_variables)
        D2_gradients = t.gradient(D2_loss,discriminator_2.trainable_variables)
        D3_gradients = t.gradient(D3_loss,discriminator_3.trainable_variables)
        
        #Lets apply gradients to the networks
        generator_1_optimizer.apply_gradients(zip(G1_gradients,generator_1.trainable_variables))
        generator_2_optimizer.apply_gradients(zip(G2_gradients,generator_2.trainable_variables))
        generator_3_optimizer.apply_gradients(zip(G3_gradients,generator_3.trainable_variables))
        
        discriminator_1_optimizer.apply_gradients(zip(D1_gradients,discriminator_1.trainable_variables))
        discriminator_2_optimizer.apply_gradients(zip(D2_gradients,discriminator_2.trainable_variables))
        discriminator_3_optimizer.apply_gradients(zip(D3_gradients,discriminator_3.trainable_variables))
        
        
#Now lets train the model
#we can just save the architecture with out weights into a JSON file.
# generator_1.to_json()
# generator_2.to_json()
# generator_3.to_json()
        
# discriminator_1.to_json()
# discriminator_2.to_json()
# discriminator_3.to_json()

for epoch in range(5):
    n = 0
    for low,high in tf.data.Dataset.zip(((gen1_ip,gen2_ip,gen2_op,ground_truth))):
        train_step(low, intermediate, high_img)
        if n%10 == 0:
            print('.',end = '')
        n+=1
    
    #We are saving the model distributions
    write_log(generator_1, epoch)
    write_log(generator_2, epoch)
    write_log(generator_3, epoch)
    
    write_log(discriminator_1, epoch)
    write_log(discriminator_2, epoch)
    write_log(discriminator_3, epoch)
    
    
    #save the checkpoints for every 5 iterations
    #checkpoints only save weights
    # We can use the json models to use these weights
    if (epoch+1)%5==0:
        checkpoint1.save(file_prefix = checkpoint_prefix1)
        checkpoint2.save(file_prefix = checkpoint_prefix2)
        checkpoint3.save(file_prefix = checkpoint_prefix3)
    #We can save the entire models and weights like this:
    # generator_1.save(generator_1.h5)
    # generator_2.save(generator_2.h5)
    # generator_3.save(generator_3.h5)
        
    # discriminator_1.save(discriminator_1.h5)
    # discriminator_2.save(discriminator_2.h5)
    # discriminator_3.save(discriminator_3.h5)
    
